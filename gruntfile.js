var config = require('./configs.js');

module.exports = function(grunt) {

    grunt.initConfig({

        // Sass
        sass: {
            dist: {
                files: {
                    'assets/css/style.min.css' : 'assets/_scss/main.scss'
                },
                options: {
                    style: 'compressed',
                    compass: config.compass,
                    sourcemap: config.sourcemap ? 'auto' : 'none'
                }
            }
        },

        // Uglify
        uglify: {
            build: {
                src: 'assets/js/scripts.js',
                dest: 'assets/js/scripts.min.js'
            }
        },

        jshint: {
            options: {
                browser: true,
                globals: {
                    jQuery: true
                },
            },
            all: ['assets/js/scripts.js']
        },

        // AutoPrefixer
        autoprefixer: {
            options: {
                browsers: ['last 100 versions'],
                map: config.sourcemap
            },
            dist: {
                files:{
                  'assets/css/style.min.css':'assets/css/style.min.css'
                }
            }
        },

        newer: {
            options: {
                override: function(details, include) {
                    if (details.task == 'sass') {
                        include(true); // Newer Sass Problem
                    }
                }
            }
        },

        // Watch files for task run
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['newer:sass:dist', 'newer:autoprefixer:dist']
            },
            js: {
                files: 'assets/js/scripts.js',
                tasks: ['newer:uglify:build', 'newer:jshint']
            }
        }
    });

    // Load Task
    require('load-grunt-tasks')(grunt);

    // Register Task
    grunt.registerTask('default', ['watch']);
}